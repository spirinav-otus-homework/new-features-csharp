using System;
using System.Collections.Generic;
using System.Linq;

namespace MetricConverterCore
{
	//NEW FEATURE - Record
	public record MetersIn(MetricUnit Unit, decimal Value);

	public class MetricRules
	{
		private readonly List<MetersIn> _rules = new();

		public void Add(MetersIn rule) => _rules.Add(rule);

		public MetersIn Get(MetricUnit unit) =>
			_rules.FirstOrDefault(x => x.Unit == unit) ?? throw new ArgumentOutOfRangeException(nameof(unit));

		public decimal GetValue(MetricUnit unit) => Get(unit).Value;
	}
}