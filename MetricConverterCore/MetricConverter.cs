﻿using System;
using System.Runtime.CompilerServices;

namespace MetricConverterCore
{
	public static class MetricConverter
	{
		//NEW FEATURE - Target-typed new expression
		private static readonly MetricRules Rules = new();

		//NEW FEATURE - Module Initializer
		[ModuleInitializer]
		public static void InitializeRules()
		{
			var metersIn = (decimal) 0.000_000_000_000_000_001;
			foreach (MetricUnit unit in Enum.GetValues(typeof(MetricUnit)))
			{
				switch (unit)
				{
					//NEW FEATURE - Relational Pattern Matching
					case MetricUnit.fm or MetricUnit.pm or MetricUnit.nm or
						MetricUnit.μm or MetricUnit.mm or MetricUnit.m or MetricUnit.km:
						Rules.Add(new MetersIn(unit, metersIn *= 1000));
						break;
					case MetricUnit.cm:
						Rules.Add(new MetersIn(unit, (decimal) 0.01));
						break;
					case MetricUnit.dm:
						Rules.Add(new MetersIn(unit, (decimal) 0.1));
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		public static decimal Convert(decimal value, MetricUnit sourceUnit, MetricUnit targetUnit)
		{
			return Rules.GetValue(sourceUnit) / Rules.GetValue(targetUnit) * value;
		}
	}
}