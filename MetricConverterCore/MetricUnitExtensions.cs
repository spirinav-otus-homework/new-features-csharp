﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace MetricConverterCore
{
	public static class MetricUnitExtensions
	{
		public static string GetDisplayName(this MetricUnit metricUnit)
		{
			var displayName = typeof(MetricUnit)
				.GetMember(metricUnit.ToString())
				.FirstOrDefault()!
				.GetCustomAttribute<DisplayAttribute>()?
				.GetName();

			return string.IsNullOrEmpty(displayName) ? metricUnit.ToString() : displayName;
		}
	}
}