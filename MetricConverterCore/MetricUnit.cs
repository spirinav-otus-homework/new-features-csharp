﻿using System.ComponentModel.DataAnnotations;

// ReSharper disable InconsistentNaming
namespace MetricConverterCore
{
	public enum MetricUnit
	{
		/// <summary>
		/// Femtometer
		/// </summary>
		[Display(Name = "Femtometer")]
		fm,

		/// <summary>
		/// Pycnometer
		/// </summary>
		[Display(Name = "Pycnometer")]
		pm,

		/// <summary>
		/// Nanometer
		/// </summary>
		[Display(Name = "Nanometer")]
		nm,

		/// <summary>
		/// Micrometer
		/// </summary>
		[Display(Name = "Micrometer")]
		μm,

		/// <summary>
		/// Millimeter
		/// </summary>
		[Display(Name = "Millimeter")]
		mm,

		/// <summary>
		/// Centimetre
		/// </summary>
		[Display(Name = "Centimetre")]
		cm,

		/// <summary>
		/// Decimeter
		/// </summary>
		[Display(Name = "Decimeter")]
		dm,

		/// <summary>
		/// Meter
		/// </summary>
		[Display(Name = "Meter")]
		m,

		/// <summary>
		/// Kilometer
		/// </summary>
		[Display(Name = "Kilometer")]
		km
	}
}