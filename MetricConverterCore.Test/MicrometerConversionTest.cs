﻿using Xunit;

namespace MetricConverterCore.Test
{
	public class MicrometerConversionTest
	{
		private decimal _valueToConvert;
		private decimal _result;

		[Fact]
		public void MetricConverter_Convert_ShouldConvertMicrometerToFemtometer()
		{
			// Arrange
			_valueToConvert = (decimal) 0.000_000_001;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.μm, MetricUnit.fm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertMicrometerToPycnometer()
		{
			// Arrange
			_valueToConvert = (decimal) 0.000_001;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.μm, MetricUnit.pm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertMicrometerToNanometer()
		{
			// Arrange
			_valueToConvert = (decimal) 0.001;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.μm, MetricUnit.nm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertMicrometerToMicrometer()
		{
			// Arrange
			_valueToConvert = 1;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.μm, MetricUnit.μm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertMicrometerToMillimeter()
		{
			// Arrange
			_valueToConvert = 1_000;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.μm, MetricUnit.mm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertMicrometerToCentimetre()
		{
			// Arrange
			_valueToConvert = 10_000;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.μm, MetricUnit.cm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertMicrometerToDecimeter()
		{
			// Arrange
			_valueToConvert = 100_000;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.μm, MetricUnit.dm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertMicrometerToMeter()
		{
			// Arrange
			_valueToConvert = 1_000_000;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.μm, MetricUnit.m);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertMicrometerToKilometer()
		{
			// Arrange
			_valueToConvert = 1_000_000_000;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.μm, MetricUnit.km);

			// Assert
			Assert.Equal(1, _result);
		}
	}
}