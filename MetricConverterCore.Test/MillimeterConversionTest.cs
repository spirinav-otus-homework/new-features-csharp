﻿using Xunit;

namespace MetricConverterCore.Test
{
	public class MillimeterConversionTest
	{
		private decimal _valueToConvert;
		private decimal _result;

		[Fact]
		public void MetricConverter_Convert_ShouldConvertMillimeterToFemtometer()
		{
			// Arrange
			_valueToConvert = (decimal) 0.000_000_000_001;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.mm, MetricUnit.fm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertMillimeterToPycnometer()
		{
			// Arrange
			_valueToConvert = (decimal) 0.000_000_001;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.mm, MetricUnit.pm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertMillimeterToNanometer()
		{
			// Arrange
			_valueToConvert = (decimal) 0.000_001;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.mm, MetricUnit.nm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertMillimeterToMicrometer()
		{
			// Arrange
			_valueToConvert = (decimal) 0.001;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.mm, MetricUnit.μm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertMillimeterToMillimeter()
		{
			// Arrange
			_valueToConvert = 1;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.mm, MetricUnit.mm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertMillimeterToCentimetre()
		{
			// Arrange
			_valueToConvert = 10;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.mm, MetricUnit.cm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertMillimeterToDecimeter()
		{
			// Arrange
			_valueToConvert = 100;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.mm, MetricUnit.dm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertMillimeterToMeter()
		{
			// Arrange
			_valueToConvert = 1_000;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.mm, MetricUnit.m);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertMillimeterToKilometer()
		{
			// Arrange
			_valueToConvert = 1_000_000;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.mm, MetricUnit.km);

			// Assert
			Assert.Equal(1, _result);
		}
	}
}