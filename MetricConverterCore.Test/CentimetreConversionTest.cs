﻿using Xunit;

namespace MetricConverterCore.Test
{
	public class CentimetreConversionTest
	{
		private decimal _valueToConvert;
		private decimal _result;

		[Fact]
		public void MetricConverter_Convert_ShouldConvertCentimetreToFemtometer()
		{
			// Arrange
			_valueToConvert = (decimal) 0.000_000_000_000_1;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.cm, MetricUnit.fm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertCentimetreToPycnometer()
		{
			// Arrange
			_valueToConvert = (decimal) 0.000_000_000_1;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.cm, MetricUnit.pm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertCentimetreToNanometer()
		{
			// Arrange
			_valueToConvert = (decimal) 0.000_000_1;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.cm, MetricUnit.nm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertCentimetreToMicrometer()
		{
			// Arrange
			_valueToConvert = (decimal) 0.000_1;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.cm, MetricUnit.μm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertCentimetreToMillimeter()
		{
			// Arrange
			_valueToConvert = (decimal) 0.1;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.cm, MetricUnit.mm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertCentimetreToCentimetre()
		{
			// Arrange
			_valueToConvert = 1;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.cm, MetricUnit.cm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertCentimetreToDecimeter()
		{
			// Arrange
			_valueToConvert = 10;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.cm, MetricUnit.dm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertCentimetreToMeter()
		{
			// Arrange
			_valueToConvert = 100;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.cm, MetricUnit.m);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertCentimetreToKilometer()
		{
			// Arrange
			_valueToConvert = 100_000;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.cm, MetricUnit.km);

			// Assert
			Assert.Equal(1, _result);
		}
	}
}