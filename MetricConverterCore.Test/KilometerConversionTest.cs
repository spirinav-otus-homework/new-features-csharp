﻿using Xunit;

namespace MetricConverterCore.Test
{
	public class KilometerConversionTest
	{
		private decimal _valueToConvert;
		private decimal _result;

		[Fact]
		public void MetricConverter_Convert_ShouldConvertKilometerToFemtometer()
		{
			// Arrange
			_valueToConvert = (decimal) 0.000_000_000_000_000_001;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.km, MetricUnit.fm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertKilometerToPycnometer()
		{
			// Arrange
			_valueToConvert = (decimal) 0.000_000_000_000_001;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.km, MetricUnit.pm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertKilometerToNanometer()
		{
			// Arrange
			_valueToConvert = (decimal) 0.000_000_000_001;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.km, MetricUnit.nm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertKilometerToMicrometer()
		{
			// Arrange
			_valueToConvert = (decimal) 0.000_000_001;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.km, MetricUnit.μm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertKilometerToMillimeter()
		{
			// Arrange
			_valueToConvert = (decimal) 0.000_001;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.km, MetricUnit.mm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertKilometerToCentimetre()
		{
			// Arrange
			_valueToConvert = (decimal) 0.000_01;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.km, MetricUnit.cm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertKilometerToDecimeter()
		{
			// Arrange
			_valueToConvert = (decimal) 0.000_1;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.km, MetricUnit.dm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertKilometerToMeter()
		{
			// Arrange
			_valueToConvert = (decimal) 0.001;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.km, MetricUnit.m);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertKilometerToKilometer()
		{
			// Arrange
			_valueToConvert = 1;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.km, MetricUnit.km);

			// Assert
			Assert.Equal(1, _result);
		}
	}
}