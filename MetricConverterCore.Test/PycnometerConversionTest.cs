﻿using Xunit;

namespace MetricConverterCore.Test
{
	public class PycnometerConversionTest
	{
		private decimal _valueToConvert;
		private decimal _result;

		[Fact]
		public void MetricConverter_Convert_ShouldConvertPycnometerToFemtometer()
		{
			// Arrange
			_valueToConvert = (decimal) 0.001;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.pm, MetricUnit.fm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertPycnometerToPycnometer()
		{
			// Arrange
			_valueToConvert = 1;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.pm, MetricUnit.pm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertPycnometerToNanometer()
		{
			// Arrange
			_valueToConvert = 1_000;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.pm, MetricUnit.nm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertPycnometerToMicrometer()
		{
			// Arrange
			_valueToConvert = 1_000_000;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.pm, MetricUnit.μm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertPycnometerToMillimeter()
		{
			// Arrange
			_valueToConvert = 1_000_000_000;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.pm, MetricUnit.mm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertPycnometerToCentimetre()
		{
			// Arrange
			_valueToConvert = 10_000_000_000;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.pm, MetricUnit.cm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertPycnometerToDecimeter()
		{
			// Arrange
			_valueToConvert = 100_000_000_000;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.pm, MetricUnit.dm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertPycnometerToMeter()
		{
			// Arrange
			_valueToConvert = 1_000_000_000_000;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.pm, MetricUnit.m);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertPycnometerToKilometer()
		{
			// Arrange
			_valueToConvert = 1_000_000_000_000_000;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.pm, MetricUnit.km);

			// Assert
			Assert.Equal(1, _result);
		}
	}
}