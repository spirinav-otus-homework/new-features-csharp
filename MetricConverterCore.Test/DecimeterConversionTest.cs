﻿using Xunit;

namespace MetricConverterCore.Test
{
	public class DecimeterConversionTest
	{
		private decimal _valueToConvert;
		private decimal _result;

		[Fact]
		public void MetricConverter_Convert_ShouldConvertDecimeterToFemtometer()
		{
			// Arrange
			_valueToConvert = (decimal) 0.000_000_000_000_01;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.dm, MetricUnit.fm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertDecimeterToPycnometer()
		{
			// Arrange
			_valueToConvert = (decimal) 0.000_000_000_01;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.dm, MetricUnit.pm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertDecimeterToNanometer()
		{
			// Arrange
			_valueToConvert = (decimal) 0.000_000_01;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.dm, MetricUnit.nm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertDecimeterToMicrometer()
		{
			// Arrange
			_valueToConvert = (decimal) 0.000_01;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.dm, MetricUnit.μm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertDecimeterToMillimeter()
		{
			// Arrange
			_valueToConvert = (decimal) 0.01;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.dm, MetricUnit.mm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertDecimeterToCentimetre()
		{
			// Arrange
			_valueToConvert = (decimal) 0.1;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.dm, MetricUnit.cm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertDecimeterToDecimeter()
		{
			// Arrange
			_valueToConvert = 1;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.dm, MetricUnit.dm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertDecimeterToMeter()
		{
			// Arrange
			_valueToConvert = 10;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.dm, MetricUnit.m);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertDecimeterToKilometer()
		{
			// Arrange
			_valueToConvert = 10_000;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.dm, MetricUnit.km);

			// Assert
			Assert.Equal(1, _result);
		}
	}
}