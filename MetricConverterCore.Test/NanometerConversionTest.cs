﻿using Xunit;

namespace MetricConverterCore.Test
{
	public class NanometerConversionTest
	{
		private decimal _valueToConvert;
		private decimal _result;

		[Fact]
		public void MetricConverter_Convert_ShouldConvertNanometerToFemtometer()
		{
			// Arrange
			_valueToConvert = (decimal) 0.000_001;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.nm, MetricUnit.fm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertNanometerToPycnometer()
		{
			// Arrange
			_valueToConvert = (decimal) 0.001;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.nm, MetricUnit.pm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertNanometerToNanometer()
		{
			// Arrange
			_valueToConvert = 1;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.nm, MetricUnit.nm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertNanometerToMicrometer()
		{
			// Arrange
			_valueToConvert = 1_000;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.nm, MetricUnit.μm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertNanometerToMillimeter()
		{
			// Arrange
			_valueToConvert = 1_000_000;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.nm, MetricUnit.mm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertNanometerToCentimetre()
		{
			// Arrange
			_valueToConvert = 10_000_000;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.nm, MetricUnit.cm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertNanometerToDecimeter()
		{
			// Arrange
			_valueToConvert = 100_000_000;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.nm, MetricUnit.dm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertNanometerToMeter()
		{
			// Arrange
			_valueToConvert = 1_000_000_000;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.nm, MetricUnit.m);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertNanometerToKilometer()
		{
			// Arrange
			_valueToConvert = 1_000_000_000_000;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.nm, MetricUnit.km);

			// Assert
			Assert.Equal(1, _result);
		}
	}
}