﻿using Xunit;

namespace MetricConverterCore.Test
{
	public class FemtometerConversionTest
	{
		private decimal _valueToConvert;
		private decimal _result;

		[Fact]
		public void MetricConverter_Convert_ShouldConvertFemtometerToFemtometer()
		{
			// Arrange
			_valueToConvert = 1;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.fm, MetricUnit.fm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertFemtometerToPycnometer()
		{
			// Arrange
			_valueToConvert = 1_000;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.fm, MetricUnit.pm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertFemtometerToNanometer()
		{
			// Arrange
			_valueToConvert = 1_000_000;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.fm, MetricUnit.nm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertFemtometerToMicrometer()
		{
			// Arrange
			_valueToConvert = 1_000_000_000;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.fm, MetricUnit.μm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertFemtometerToMillimeter()
		{
			// Arrange
			_valueToConvert = 1_000_000_000_000;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.fm, MetricUnit.mm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertFemtometerToCentimetre()
		{
			// Arrange
			_valueToConvert = 10_000_000_000_000;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.fm, MetricUnit.cm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertFemtometerToDecimeter()
		{
			// Arrange
			_valueToConvert = 100_000_000_000_000;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.fm, MetricUnit.dm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertFemtometerToMeter()
		{
			// Arrange
			_valueToConvert = 1_000_000_000_000_000;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.fm, MetricUnit.m);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertFemtometerToKilometer()
		{
			// Arrange
			_valueToConvert = 1_000_000_000_000_000_000;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.fm, MetricUnit.km);

			// Assert
			Assert.Equal(1, _result);
		}
	}
}