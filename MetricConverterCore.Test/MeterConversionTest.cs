﻿using Xunit;

namespace MetricConverterCore.Test
{
	public class MeterConversionTest
	{
		private decimal _valueToConvert;
		private decimal _result;

		[Fact]
		public void MetricConverter_Convert_ShouldConvertMeterToFemtometer()
		{
			// Arrange
			_valueToConvert = (decimal) 0.000_000_000_000_001;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.m, MetricUnit.fm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertMeterToPycnometer()
		{
			// Arrange
			_valueToConvert = (decimal) 0.000_000_000_001;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.m, MetricUnit.pm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertMeterToNanometer()
		{
			// Arrange
			_valueToConvert = (decimal) 0.000_000_001;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.m, MetricUnit.nm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertMeterToMicrometer()
		{
			// Arrange
			_valueToConvert = (decimal) 0.000_001;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.m, MetricUnit.μm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertMeterToMillimeter()
		{
			// Arrange
			_valueToConvert = (decimal) 0.001;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.m, MetricUnit.mm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertMeterToCentimetre()
		{
			// Arrange
			_valueToConvert = (decimal) 0.01;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.m, MetricUnit.cm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertMeterToDecimeter()
		{
			// Arrange
			_valueToConvert = (decimal) 0.1;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.m, MetricUnit.dm);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertMeterToMeter()
		{
			// Arrange
			_valueToConvert = 1;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.m, MetricUnit.m);

			// Assert
			Assert.Equal(1, _result);
		}

		[Fact]
		public void MetricConverter_Convert_ShouldConvertMeterToKilometer()
		{
			// Arrange
			_valueToConvert = 1_000;

			// Act
			_result = MetricConverter.Convert(_valueToConvert, MetricUnit.m, MetricUnit.km);

			// Assert
			Assert.Equal(1, _result);
		}
	}
}