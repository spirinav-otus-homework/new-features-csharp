﻿using System;

using MetricConverterCore;

// ReSharper disable RedundantAssignment
namespace ConsoleApp
{
	public static class InputHelper
	{
		private static void ShowWarningMessage() => Console.WriteLine("Incorrect input. Try again.");

		public static bool Validate(object input, Func<object, bool> func)
		{
			if (func.Invoke(input))
			{
				return true;
			}

			ShowWarningMessage();
			return false;
		}

		public static bool Parse(string input, ref decimal value)
		{
			if (decimal.TryParse(input, out value))
			{
				return true;
			}

			ShowWarningMessage();
			return false;
		}

		public static bool Parse(string input, ref MetricUnit unit)
		{
			if (!decimal.TryParse(input, out _) && Enum.TryParse(input, out unit))
			{
				return true;
			}

			ShowWarningMessage();
			return false;
		}
	}
}