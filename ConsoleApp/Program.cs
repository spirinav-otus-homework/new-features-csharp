﻿using System;

using ConsoleApp;

using MetricConverterCore;

//NEW FEATURE - Top-level statements
while (true)
{
	Console.WriteLine("Enter the value to convert (for example \"1 km\"):");

	var input = Console.ReadLine();
	if (!InputHelper.Validate(input, i => i is string str && !string.IsNullOrWhiteSpace(str)))
		continue;

	var enterArray = input?.Split(' ');
	if (!InputHelper.Validate(enterArray, e => e is string[] {Length: >= 2}))
		continue;

	decimal value = 0;
	if (!InputHelper.Parse(enterArray?[0], ref value))
		continue;

	MetricUnit sourceUnit = default;
	if (!InputHelper.Parse(enterArray?[1], ref sourceUnit))
		continue;

	while (true)
	{
		Console.WriteLine("Enter the target metric unit (for example \"cm\"):");

		input = Console.ReadLine();
		if (!InputHelper.Validate(input, i => i is string str && !string.IsNullOrWhiteSpace(str)))
			continue;

		MetricUnit targetUnit = default;
		if (!InputHelper.Parse(input, ref targetUnit))
			continue;

		var conversionResult = MetricConverter.Convert(value, sourceUnit, targetUnit);

		Console.WriteLine($"{value} {sourceUnit.GetDisplayName().ToLower() + "s"} is equal to " +
		                  $"{conversionResult:G29} {targetUnit.GetDisplayName().ToLower() + "s"}.");

		break;
	}

	Console.ReadLine();
}